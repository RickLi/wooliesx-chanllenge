# WooliesXApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.0.4.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## My notes

I used ngrx as my store mangement tool. I wrote some test cases regarding components, services, effects, selectors, actions, reducers as well as some e2e tests. With the time limitation, maybe not all test cases are fully cover. But this should be enough to show to purpose of the test. 

I used jasmine-marbles to test some rxjs operations. The project has mainly two modules, product & checkout. This project use the latest angular cli 9.0.4

Hope Omar likes my solution
