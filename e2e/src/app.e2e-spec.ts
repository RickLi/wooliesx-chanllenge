import { AppPage } from './app.po';
import { browser, logging } from 'protractor';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getTitleText()).toEqual('WooliesX Shopping App');
  });

  it('product list section should have product', () => {
    browser.waitForAngular();
    browser.driver.sleep(500);
    const productList = page.getProductList();
    expect(productList).toEqual(6);
  });

  it('should be able to click add to cart button', () => {
    page.clickAdd(1);
    browser.driver.sleep(500);
    page.clickAdd(2);
    browser.driver.sleep(500);
    page.clickAdd(3);
    browser.driver.sleep(500);
    page.clickAdd(4);
    browser.driver.sleep(500);
    page.clickAdd(5);
    browser.driver.sleep(500);
    page.clickAdd(6);
  });

  it('should be able to click checkout button', () => {
    page.checkout();
    browser.driver.sleep(2000);
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
