import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo(): Promise<unknown> {
    return browser.get(browser.baseUrl) as Promise<unknown>;
  }

  getTitleText(): Promise<string> {
    return element(by.css('app-root .mat-toolbar span')).getText() as Promise<string>;
  }

  getProductList() {
    return element.all(by.css('.productItem')).count();
  }

  clickAdd(nth) {
    browser.actions().click(element.all(by.css('.addToCart')).get(nth - 1)).perform();
  }

  checkout() {
    browser.actions().click(element(by.id('checkout'))).perform();
  }
}
