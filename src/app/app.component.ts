import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { MatDialog } from '@angular/material/dialog';
import { State } from './../store';
import { getProductsCount } from './../store/product/product.selectors';
import { CheckoutComponent } from './checkout/checkout.component';
import * as CheckoutActions from './../store/checkout/checkout.actions';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'WooliesX Shopping App';
  count = 0;
  constructor(
    private store: Store<State>,
    private dialog: MatDialog
  ) {
    this.readCheckoutItemsCount();
  }

  checkout(): void {
    const dialogRef = this.dialog.open(CheckoutComponent, {
      width: '80%',
      data: {}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      console.log(result);
      if (result) {
        // const action = new CheckoutActions.AddBookAction(result);
        // this.store.dispatch(action);
      }
    });
  }

  private readCheckoutItemsCount() {
    this.store.select(getProductsCount)
      .subscribe(count => {
        this.count = count;
      });
  }
}
