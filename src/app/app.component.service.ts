import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Product } from './../store/product/product.model';
import { Checkout } from './../store/checkout/checkout.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private TOKEN = 'dff839d23bb8c18a212aabe67383320be57d';
  private HOST = `https://wooliesxfechallenge.azurewebsites.net/api/v1/resources`;
  constructor(private http: HttpClient) {

  }

  loadProducts(): Observable<Product[]> {
    return this.http.get<Product[]>(`${this.HOST}/products?token=${this.TOKEN}`);
  }
}

@Injectable({
  providedIn: 'root'
})
export class CheckoutService {
  private TOKEN = 'dff839d23bb8c18a212aabe67383320be57d';
  private HOST = `https://wooliesxfechallenge.azurewebsites.net/api/v1/resources`;
  constructor(private http: HttpClient) {

  }

  addToCart(checkout: Checkout): BehaviorSubject<Checkout> {
    const list = JSON.parse(localStorage.getItem('ngrx-checkout-list')) || [];
    const product = list.find(d => d.productId === checkout.productId);
    let newList = [];
    if (product) {
      newList = list.map((p) => {
        if (p.productId === checkout.productId) {
          return Object.assign({}, p, {
            count: ++p.count
          });
        }
        return p;
      });
    } else {
      newList = [
        ...list,
      ].concat(checkout);
    }
    localStorage.setItem('ngrx-checkout-list', JSON.stringify(newList.length === 0 ? [checkout] : newList));
    return new BehaviorSubject(checkout);
  }

  loadCheckout(): BehaviorSubject<Checkout[]> {
    const items = localStorage.getItem('ngrx-checkout-list');
    return new BehaviorSubject(items ? JSON.parse(items) : []);
  }

  checkout(): any {
    const items = localStorage.getItem('ngrx-checkout-list');
    return this.http.post<any>(`${this.HOST}/checkout?token=${this.TOKEN}`,
      items ? JSON.parse(items) : [],
      { responseType: 'text' as 'json' }
    );
  }
}
