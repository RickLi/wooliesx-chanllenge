import { TestBed, async, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { CheckoutService, ProductService } from './app.component.service';

describe('App.ComponentService', () => {
  let productService: ProductService;
  let checkoutService: CheckoutService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
      ],
      providers: [
        CheckoutService,
        ProductService
      ],
    });
    productService = TestBed.inject(ProductService);
    checkoutService = TestBed.inject(CheckoutService);
  });

  it('should be created', () => {
    expect(productService).toBeTruthy();
    expect(checkoutService).toBeTruthy();
  });

  it(`should fetch posts as an Observable`, async(inject([HttpTestingController, ProductService],
    (httpMock: HttpTestingController) => {

    const postItem = [
      {
        productId: 1,
        stockOnHand: 1,
        audPrice: 1,
        description: 'd2',
        name: 'p2'
      },
      {
        productId: 1,
        stockOnHand: 2,
        audPrice: 2,
        description: 'qui est esse',
        name: 'p1'
      }
    ];
    productService.loadProducts()
      .subscribe((products: any) => {
        expect(products.length).toBe(2);
      });

    // tslint:disable-next-line: max-line-length
    const req = httpMock.expectOne('https://wooliesxfechallenge.azurewebsites.net/api/v1/resources/products?token=dff839d23bb8c18a212aabe67383320be57d');
    expect(req.request.method).toBe('GET');

    req.flush(postItem);
    httpMock.verify();

  })));
});
