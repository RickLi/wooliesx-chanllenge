import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { State } from './../../store';
import { Observable } from 'rxjs';
import { getTotalAmount } from '../../store/checkout/checkout.selectors';
import { Checkout } from './../../store/checkout/checkout.model';
import { getCheckoutProducts } from './../../store/product/product.selectors';
import * as CheckoutActions from './../../store/checkout/checkout.actions';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss']
})
export class CheckoutComponent implements OnInit {
  items$: Observable<Checkout[]>;
  total: number;

  constructor(
    public dialogRef: MatDialogRef<CheckoutComponent>,
    private store: Store<State>,
  ) {
  }

  ngOnInit() {
    this.items$ = this.store.select(getCheckoutProducts);
    this.store.select(getTotalAmount)
      .subscribe(total => {
        this.total = total;
      });
  }

  onCancelClick(): void {
    this.dialogRef.close();
  }

  checkout(): void {
    if (confirm('Confirm to send the order ?')) {
      this.store.dispatch(new CheckoutActions.SubimitCheckoutsAction());
      this.dialogRef.close();
    } else {
      this.dialogRef.close();
    }
  }

}
