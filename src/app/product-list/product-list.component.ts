import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Store } from '@ngrx/store';
import { State } from './../../store';
import * as ProductActions from './../../store/product/product.actions';
import * as CheckoutActions from './../../store/checkout/checkout.actions';
import { Product } from './../../store/product/product.model';
import { getProducts } from './../../store/product/product.selectors';
import { Observable } from 'rxjs';
import { transition, style, animate, trigger } from '@angular/animations';

export const DROP_ANIMATION = trigger('drop', [
  transition(':enter', [
    style({ transform: 'translateY(-200px)', opacity: 0 }),
    animate(
      '300ms cubic-bezier(1.000, 0.000, 0.000, 1.000)',
      style({ transform: 'translateY(0)', opacity: 1 })
    ),
  ]),
  transition(':leave', [
    style({ transform: 'translateY(0)', opacity: 1 }),
    animate(
      '200ms cubic-bezier(1.000, 0.000, 0.000, 1.000)',
      style({ transform: 'translateY(-200px)', opacity: 0 })
    ),
  ]),
]);

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss'],
  animations: [DROP_ANIMATION],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProductListComponent implements OnInit {
  products$: Observable<Product[]>;

  constructor(
    private store: Store<State>,
  ) {
    this.store.dispatch(new CheckoutActions.LoadCheckoutAction());
    this.store.dispatch(new ProductActions.PopulateProductsAction());
  }

  ngOnInit() {
    this.products$ = this.store.select(getProducts);
  }

  add(product) {
    this.store.dispatch(new CheckoutActions.AddCheckoutAction(product.productId, product.audPrice, product.name));
  }
}
