import { Injectable } from '@angular/core';
import { Checkout } from './checkout.model';
import { Observable, of } from 'rxjs';
import { mergeMap, map, catchError, switchMap } from 'rxjs/operators';
import { CheckoutService } from '../../app/app.component.service';

/* NgRx */
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import * as checkoutActions from './checkout.actions';

@Injectable()
export class CheckoutEffects {

  constructor(private actions$: Actions,
              private checkoutService: CheckoutService
  ) { }


  @Effect()
  addProduct$: Observable<Action> = this.actions$.pipe(
    ofType(checkoutActions.ADD_PRODUCT),
    map((action: checkoutActions.AddCheckoutAction) => {
      return {
        productId: action.productId,
        price: action.audPrice,
        name: action.name,
        count: 1
      };
    }),
    switchMap((checkout: Checkout) =>
    this.checkoutService.addToCart(checkout)
        .pipe(
          map(product => (new checkoutActions.AddCheckoutActionSuccess(product))),
          catchError(err => of(new checkoutActions.AddCheckoutActionFail(err)))
        )
    )
  );

  @Effect()
  loadCheckoutItems$: Observable<Action> = this.actions$.pipe(
    ofType(checkoutActions.LOAD_CHECKOUT),
    mergeMap(() =>
      this.checkoutService.loadCheckout().pipe(
        map(items => {
          return (new checkoutActions.LoadCheckoutActionSuccess(items));
        }),
        catchError(err => of(new checkoutActions.LoadCheckoutActionFail(err)))
      )
    )
  );

  @Effect()
  checkout$: Observable<any> = this.actions$.pipe(
    ofType(checkoutActions.SUBIMIT_CHECKOUT),
    switchMap(() =>
      this.checkoutService.checkout().pipe(
        map(status => {
          localStorage.removeItem('ngrx-checkout-list');
          return (new checkoutActions.SubimitCheckoutsActionSuccess(status));
        }),
        catchError(err => of(new checkoutActions.SubimitCheckoutsActionFail(err)))
      )
    )
  );
}
