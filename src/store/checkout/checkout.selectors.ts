import { createSelector } from '@ngrx/store';
import { State } from '../index';
import { Checkout } from './checkout.model';

export const getCheckoutProducts = (state: State) => state.checkout;

export const getTotalAmount = createSelector(getCheckoutProducts, (selectedProducts: Checkout[]) => {
  return selectedProducts ? selectedProducts.reduce((accu, curr) => accu + curr.count * curr.price, 0) : 0;
});
