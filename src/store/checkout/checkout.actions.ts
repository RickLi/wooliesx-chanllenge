import { Action } from '@ngrx/store';
import { Checkout } from './checkout.model';

export const ADD_PRODUCT = '[Checkout] add';
export const LOAD_CHECKOUT = '[Checkout] load';
export const ADD_PRODUCT_FAIL = '[Checkout] add fail';
export const LOAD_CHECKOUT_FAIL = '[Checkout] load fail';
export const ADD_PRODUCT_SUCCESS = '[Checkout] add success';
export const LOAD_CHECKOUT_SUCCESS = '[Checkout] load success';
export const SUBIMIT_CHECKOUT = '[Checkout] sumit';
export const SUBIMIT_CHECKOUT_FAIL = '[Checkout] submit Fail';
export const SUBIMIT_CHECKOUT_SUCCESS = '[Checkouts] submit success';

export class AddCheckoutAction implements Action {
  readonly type = ADD_PRODUCT;
  constructor(public productId: string, public audPrice: number, public name: string) { }
}

export class AddCheckoutActionFail implements Action {
  readonly type = ADD_PRODUCT_FAIL;
  constructor(public payload: any) { }
}

export class AddCheckoutActionSuccess implements Action {
  readonly type = ADD_PRODUCT_SUCCESS;
  constructor(public payload: Checkout) { }
}

export class LoadCheckoutAction implements Action {
  readonly type = LOAD_CHECKOUT;
}

export class LoadCheckoutActionFail implements Action {
  readonly type = LOAD_CHECKOUT_FAIL;
  constructor(public payload: any) { }
}

export class LoadCheckoutActionSuccess implements Action {
  readonly type = LOAD_CHECKOUT_SUCCESS;
  constructor(public payload: Checkout[]) { }
}


export class SubimitCheckoutsAction implements Action {
  readonly type = SUBIMIT_CHECKOUT;
}

export class SubimitCheckoutsActionFail implements Action {
  readonly type = SUBIMIT_CHECKOUT_FAIL;
  constructor(public payload: any) { }
}

export class SubimitCheckoutsActionSuccess implements Action {
  readonly type = SUBIMIT_CHECKOUT_SUCCESS;
  constructor(public payload: any) { }
}


export type CheckoutActionType =
  AddCheckoutAction |
  AddCheckoutActionSuccess |
  AddCheckoutActionFail |
  LoadCheckoutAction |
  LoadCheckoutActionSuccess |
  LoadCheckoutActionFail |
  SubimitCheckoutsAction |
  SubimitCheckoutsActionSuccess |
  SubimitCheckoutsActionFail;
