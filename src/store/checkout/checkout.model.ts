export interface Checkout {
  productId: string;
  price: number;
  count: number;
}
