import * as CheckoutActions from './checkout.actions';
import { Checkout } from './checkout.model';

const initialState: Checkout[] = [];

export function CheckoutReducer(state: Checkout[] = initialState, action: CheckoutActions.CheckoutActionType) {
  switch (action.type) {
    case CheckoutActions.ADD_PRODUCT_SUCCESS: {
      const product = state.find(d => d.productId === action.payload.productId);
      if (product) {
        return state.map((p) => {
          if (p.productId === action.payload.productId) {
            return Object.assign({}, p, {
              count: ++p.count
            });
          }
          return p;
        });
      } else {
        return [
          ...state,
        ].concat(action.payload);
      }
    }

    case CheckoutActions.LOAD_CHECKOUT_SUCCESS: {
      return [...state, ...action.payload];
    }

    case CheckoutActions.SUBIMIT_CHECKOUT_SUCCESS: {
      return initialState;
    }

    default: {
      return state;
    }
  }
}
