import {
  ActionReducerMap,
  MetaReducer
} from '@ngrx/store';
import { environment } from '../environments/environment';
import { Checkout } from './checkout/checkout.model';
import { Product } from './product/product.model';
import { ProductReducer } from './product/product.reducer';
import { CheckoutReducer } from './checkout/checkout.reducer';
import { ProductEffects } from './product/product.effects';
import { CheckoutEffects } from './checkout/checkout.effects';

export interface State {
  products: Product[];
  checkout: Checkout[];
}

export const reducers: ActionReducerMap<State> = {
  products: ProductReducer,
  checkout: CheckoutReducer,
};

export const effects: any[] = [ProductEffects, CheckoutEffects];

export const metaReducers: MetaReducer<State>[] = !environment.production ? [] : [];
