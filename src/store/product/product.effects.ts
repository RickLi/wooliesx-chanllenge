import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';
import { mergeMap, map, catchError, switchMap } from 'rxjs/operators';
import { ProductService } from '../../app/app.component.service';

/* NgRx */
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import * as productActions from './product.actions';

@Injectable()
export class ProductEffects {

  constructor(private actions$: Actions,
              private productService: ProductService
    ) { }

  @Effect()
  loadProducts$: Observable<Action> = this.actions$.pipe(
    ofType(productActions.POPULATE_PRODUCTS),
    mergeMap(() =>
      this.productService.loadProducts().pipe(
        map(products => {
          return (new productActions.PopulateProductsActionSuccess(products));
        }),
        catchError(err => of(new productActions.PopulateProductsActionFail(err)))
      )
    )
  );
}
