import { Action } from '@ngrx/store';
import { Product } from './product.model';

export const POPULATE_PRODUCTS = '[Product] populate';
export const POPULATE_PRODUCTS_FAIL = '[Product] populate Fail';
export const POPULATE_PRODUCTS_SUCCESS = '[Products] populate success';

export class PopulateProductsAction implements Action {
  readonly type = POPULATE_PRODUCTS;
}

export class PopulateProductsActionFail implements Action {
  readonly type = POPULATE_PRODUCTS_FAIL;
  constructor(public payload: any) { }
}

export class PopulateProductsActionSuccess implements Action {
  readonly type = POPULATE_PRODUCTS_SUCCESS;
  constructor(public payload: Product[]) { }
}


export type ProductActionType =
  PopulateProductsAction |
  PopulateProductsActionSuccess |
  PopulateProductsActionFail;
