import * as ProductSelector from './product.selectors';
import { State } from '../index';
import { Product } from './product.model';
describe('Redux: ProductSelector', () => {

  describe('Test for getState', () => {

    it('should return the state', () => {
      const state: State = {
        products: [],
        checkout: []
      };
      const rta = ProductSelector.getState(state);
      expect(rta).toEqual(state);
    });

  });

  describe('Test for getProducts', () => {

    it('should return the products', () => {
      const state: State = {
        products: [],
        checkout: []
      };
      const rta = ProductSelector.getProducts(state);
      expect(rta).toEqual(state.products);
    });

  });

  describe('Test for getProductsCount', () => {

    it('should return the correct checkout product items length', () => {
      const state: State = {
        products: [],
        checkout: [
          {
            productId: '122',
            count: 5,
            price: 1
          },
          {
            productId: '121',
            count: 5,
            price: 2
          }
        ],
      };
      const rta = ProductSelector.getProductsCount(state);
      expect(rta).toEqual(10);
    });

  });

});
