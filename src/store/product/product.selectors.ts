import { createSelector } from '@ngrx/store';
import { State } from '../index';
import { Checkout } from '../checkout/checkout.model';

export const getState = (state: State) => state;
export const getProducts = (state: State) => state.products;
export const getCheckoutProducts = (state: State) => state.checkout;

export const getProductsCount = createSelector(getCheckoutProducts, (selectedProducts: Checkout[]) => {
  return selectedProducts ? selectedProducts.reduce((accu, curr) => accu + curr.count, 0) : 0;
});
