import * as ProductActions from './product.actions';
import { Product } from './product.model';

const initialState: Product[] = [];

export function ProductReducer(state: Product[] = initialState, action: ProductActions.ProductActionType) {
  switch (action.type) {

    case ProductActions.POPULATE_PRODUCTS_SUCCESS: {
      return [...state, ...action.payload];
    }

    default: {
      return state;
    }
  }
}
