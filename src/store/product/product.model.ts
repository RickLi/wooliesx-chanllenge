export interface Product {
  productId: string;
  name: string;
  description: string;
  audPrice: number;
  stockOnHand: number;
}
