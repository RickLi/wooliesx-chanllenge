import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

import { Actions } from '@ngrx/effects';
import { hot, cold } from 'jasmine-marbles';
import { Observable } from 'rxjs';
import { empty } from 'rxjs';
import { of } from 'rxjs';

import { ProductService } from '../../app/app.component.service';
import * as productEffects from './product.effects';
import * as productActions from './product.actions';

export class TestActions extends Actions {
  constructor() {
    super(empty());
  }

  set stream(source: Observable<any>) {
    this.source = source;
  }
}

export function getActions() {
  return new TestActions();
}

describe('ProductsEffects', () => {
  let actions$: TestActions;
  let service: ProductService;
  let effects: productEffects.ProductEffects;

  const products = [{
    productId: '121',
    name: 'p1',
    description: 't1',
    audPrice: 1,
    stockOnHand: 10
  }, {
    productId: '122',
    name: 'p2',
    description: 'd2',
    audPrice: 1,
    stockOnHand: 10
  }];

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        ProductService,
        productEffects.ProductEffects,
        { provide: Actions, useFactory: getActions },
      ],
    });

    // tslint:disable-next-line: deprecation
    actions$ = TestBed.get(Actions);
    service = TestBed.inject(ProductService);
    effects = TestBed.inject(productEffects.ProductEffects);

    spyOn(service, 'loadProducts').and.returnValue(of(products));
  });

  describe('loadProducts$', () => {
    it('should return a collection from PopulateProductSuccess', () => {
      const action = new productActions.PopulateProductsAction();
      const completion = new productActions.PopulateProductsActionSuccess(products);

      actions$.stream = hot('-a', { a: action });
      const expected = cold('-b', { b: completion });

      expect(effects.loadProducts$).toBeObservable(expected);
    });
  });

});
