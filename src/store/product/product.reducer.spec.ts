import * as ProductsAction from './product.actions';
import { ProductReducer } from './product.reducer';
describe('Redux: ProductReducer', () => {
  it('should add new product to existing products array: PopulateBooksAction', () => {
    const newProducts = [{
      productId: '122',
      name: 'testNew',
      description: 'testNew',
      audPrice: 1,
      stockOnHand: 10
    }];
    const action = new ProductsAction.PopulateProductsActionSuccess(newProducts);
    const oldState = [{
      productId: '121',
      name: 'testOld',
      description: 'testOld',
      audPrice: 1,
      stockOnHand: 10
    }];
    const newState = ProductReducer(oldState, action);
    expect(newState.length).toEqual(2);
  });

});
