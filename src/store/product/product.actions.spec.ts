import * as ProductActions from './product.actions';
describe('Redux: ProductActions', () => {
  describe('Test for PopulateProductsAction', () => {
    it('should return an action with product', () => {
      const action = new ProductActions.PopulateProductsAction();
      expect(action.type).toEqual(ProductActions.POPULATE_PRODUCTS);
      const actionSuccess = new ProductActions.PopulateProductsActionSuccess([]);
      expect(actionSuccess.payload).toEqual([]);
    });
  });

});
